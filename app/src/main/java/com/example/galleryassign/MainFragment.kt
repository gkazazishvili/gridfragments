package com.example.galleryassign

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.navigation.Navigation.findNavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.galleryassign.databinding.FragmentMainBinding
import com.google.android.material.snackbar.Snackbar


class MainFragment : Fragment() {


    private var binding: FragmentMainBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMainBinding.inflate(inflater, container, false)

        fetchData()



        return binding?.root


    }

    val images = mutableListOf<Int>(
        R.drawable.first,
        R.drawable.second,
        R.drawable.third,
        R.drawable.fourth,
        R.drawable.first,
        R.drawable.second,
        R.drawable.third,
        R.drawable.fourth
    )




    fun fetchData(){
        val images = mutableListOf<Int>(
            R.drawable.first,
            R.drawable.second,
            R.drawable.third,
            R.drawable.fourth,
            R.drawable.first,
            R.drawable.second,
            R.drawable.third,
            R.drawable.fourth
        )

        val title = mutableListOf<String>(
            "Hot",
            "Leqso",
            "Guram",
            "Soso",
            "Hot",
            "Leqso",
            "Guram",
            "Soso"

        )

        val description = mutableListOf<String>(
            "Dog",
            "Nijaradze",
            "Jinoria",
            "Egadze",
            "Hot",
            "Leqso",
            "Guram",
            "Soso"
        )


        val recyclerView: RecyclerView? = binding?.recyclerView
        val adaptor = Adaptor(images)
        recyclerView?.layoutManager = GridLayoutManager(context, 3)
        recyclerView?.adapter = adaptor

        adaptor.setOnItemClickListener(object : Adaptor.onItemClickListener{
            override fun onItemClick(position: Int) {
                Toast.makeText(context, "$position", Toast.LENGTH_SHORT).show()
                val action = MainFragmentDirections.actionMainFragmentToItemFragment(title[position], description[position], position)
                    findNavController().navigate(action)
            }
        })

    }




}
