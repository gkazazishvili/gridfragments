package com.example.galleryassign

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.example.galleryassign.databinding.FragmentItemBinding
import com.example.galleryassign.databinding.FragmentMainBinding


class ItemFragment : Fragment() {


    private var binding: FragmentItemBinding? = null
    private lateinit var mainFrag: MainFragment
    private val args: ItemFragmentArgs by navArgs()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentItemBinding.inflate(inflater, container, false)

        mainFrag = MainFragment()
        binding!!.title.text = args.title
        binding!!.description.text = args.description
        val pos = args.position
            binding!!.itemImage.setImageResource(mainFrag.images[pos])

        return binding?.root
        }
    }
