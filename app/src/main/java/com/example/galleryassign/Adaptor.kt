package com.example.galleryassign

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView

class Adaptor(private val item: MutableList<Int>) : RecyclerView.Adapter<Adaptor.ViewHolder>() {

    private lateinit var mListener: onItemClickListener

    interface onItemClickListener{
        fun onItemClick(position: Int)
    }

    fun setOnItemClickListener(listener: onItemClickListener){
        mListener = listener
    }




    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_rv, parent, false)
            return ViewHolder(itemView, mListener)
    }

    override fun onBindViewHolder(holder: Adaptor.ViewHolder, position: Int) {
        holder.img.setImageResource(item[position])
//        holder.itemView.setOnClickListener(object: View.OnClickListener{
//            override fun onClick(v: View?) {
//                val activity = v!!.context as AppCompatActivity
//                val itemFragment = ItemFragment()
//
//            }
//        })
    }

    override fun getItemCount(): Int {
        return item.size
    }

    class ViewHolder(ItemView: View, listener: onItemClickListener):RecyclerView.ViewHolder(ItemView){
        val img:ImageView = itemView.findViewById(R.id.image_rv_item)

        init{
            itemView.setOnClickListener {
                listener.onItemClick(adapterPosition)
            }
        }
    }
}